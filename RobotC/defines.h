#define TICK_DELAY 2 // ms

#define VELOCITY_MAX 100
#define VELOCITY_MIN 0

#define MEASURE_ERROR 3
#define MEASURE_EQUAL_DIAPASON 1
#define MEASURE_COUNT_PERIOD 1000

#define DES_DISTANCE_MIN (40 + MEASURE_ERROR)
#define DES_DISTANCE_MAX (50 - MEASURE_ERROR)

#define DISTANCE_MIN 0
#define DISTANCE_MAX 255

#define K_MAGIC 0.60
#define K_CHUT_CHUT 0.10

                        //ms    //ms
#define DECAY_TIME_OF_K (3000 / TICK_DELAY) // ticks
