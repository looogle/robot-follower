void goLeftMotor(short power)
{
	motor[LEFT_MOTOR] = power;
}

void goRightMotor(short power)
{
	motor[RIGHT_MOTOR] = power;
}

void offSensor(tSensors sensor)
{
	SensorType[sensor] = sensorTouch;
}

void onSensor(tSensors sensor)
{
	SensorType[sensor] = sensorSONAR;
}
